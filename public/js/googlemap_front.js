var markers = [];
//var loc_x = 50.463624; //Киев
//var loc_y = 30.514526;
var init = false;

function show_map(x, y){
    clearOverlays();
    setLocation(x, y);
    if(!init) initialize_googlemaps();
    init = true;
    var myLatlng = new google.maps.LatLng(x,y);
    placeMarker(myLatlng);
}

function initialize_googlemaps() {
    var myLatlng = new google.maps.LatLng(loc_x,loc_y);
    var mapOptions = {
        zoom: 15,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    placeMarker(myLatlng);
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
    });
    
}
function setLocation(x, y){
    loc_x = x;
    loc_y = y;
}

function placeMarker(location) {
    clearOverlays();
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        icon: '/images/map-icon.png'
    });
    markers.push(marker);
    map.setCenter(location);
}
        // Sets the map on all markers in the array.
function setAllMap(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearOverlays() {
    setAllMap(null);
}

//google.maps.event.addDomListener(window, 'load', initialize_googlemaps);


