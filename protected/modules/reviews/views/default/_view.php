<?php
/* @var $this DefaultController */
/* @var $data Reviews */
?>

<div class="view" id="<?= $data->id ?>">
    <?php if($data->type == 'private' && $data->file): ?>
        <div class="item-img">
            <?= CHtml::image($data->imageThumbUrl, '') ?>
            <div class="lbl"></div>
        </div>
    <?php endif; ?>
    <div class="item-txt">
        <h3><?= $data->title ?></h3>
        <div class="txt"><?= $data->text ?></div>
        <div class="author"><?= $data->contact ?></div>
        <?= CHtml::link('', $data->fileUrl, array(
            'target' => '_blank',
            'class' => 'download-link',
        )) ?>
    </div>
    <div class="clearfix"></div>
</div>