<?php
/* @var $this DefaultController */
/* @var $model Statpages */
/* @var $form CActiveForm */
?>

<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?= $this->pageTitle; ?></h1>
    </div>
</div>

<div class="row-fluid" id="grid">
    <?php $form=$this->beginWidget('AdminForm', array(
        'id'=>'candidates-form',
        'type' => 'horizontal',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'), 
    )); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldRow($model, 'title', array('class'=>'span5')); ?>
    <?= $form->dropDownListRow($model, 'type', $model->types, array(
        'empty' => '',
        'class'=>'span5'
        )); ?>
    <?php $display = ($model->type == 'private') ? '' : 'display:none'; ?>
    <?= $form->imageFieldRow($model, 'img'); ?>
    <?= $form->fileFieldRow($model, 'file'); ?>
    <?php if($model->file): ?>
        <div class='control-group'><div class='controls'>
            <?= CHtml::link($model->file, $model->fileUrl, array('target' => '_blank')) ?>
        </div></div>
    <?php endif; ?>
    <?= $form->textAreaRow($model, 'text', array('class'=>'span7', 'rows' => 10)); ?>
    <?= $form->textFieldRow($model, 'contact', array('class'=>'span5')); ?>
    
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Сохранить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Применить',
            'htmlOptions' => array('name' => 'apply'))
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>