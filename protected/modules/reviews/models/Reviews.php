<?php

/**
 * This is the model class for table "reviews".
 *
 * The followings are the available columns in table 'reviews':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $type
 * @property string $img
 * @property string $text
 * @property string $file
 * @property string $contact
 */
class Reviews extends CActiveRecord
{
        public $types = array(
            'company' => 'Компания',
            'private' => 'Частное лицо',
        );
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Reviews the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reviews';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//                        array('title, text, type', 'required'),
			array('title, img, file, contact', 'length', 'max'=>45),
			array('type', 'length', 'max'=>7),
			array('create_time, update_time, text', 'safe'),
                        array('img', 'file', 
                            'types' => 'gif, png, jpg, jpeg',
                            'allowEmpty' => true,
                        ),
//                        array('file', 'file',
//                            'types' => 'pdf',
//                            'allowEmpty' => true,
//                            'maxSize' => 5 * 1024 * 1024,
//                        ),
                        array('text', 'length', 'max' => 500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, create_time, update_time, title, type, img, text, file, contact', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'title' => 'Название компании / частного лица',
			'type' => 'Тип',
			'img' => 'Логотип',
			'text' => 'Текст',
			'file' => 'Документ',
			'contact' => 'Контактное лицо',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('contact',$this->contact,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function behaviors()
        {
            return array(
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                ),
                'uploadableFile'=>array(
                    'class'=>'application.components.UploadableFileBehavior',
                    'fileTypes' => 'pdf',
                    'savePathAlias' => 'webroot.uploaded.review_files',
                ),
                'uploadableImage'=>array(
                    'class'=>'application.components.UploadableImageBehavior',
                    'savePathAlias' => 'webroot.uploaded.review',
                    'thumbWidth' => 174,
                ),
            );
        }
        
        public function getTypeTitle()
        {
            return $this->types[$this->type];
        }
        
        public function frontsearch()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('title', $this->title, true, 'OR');
                $criteria->compare('text', $this->text, true, 'OR');
                $criteria->compare('contact', $this->contact, true, 'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'id DESC'
                        )
		));
	}
}   