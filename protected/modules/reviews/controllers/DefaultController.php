<?php

class DefaultController extends Controller
{
	public function filters()
	{
		return array(
                    'accessControl', // perform access control for CRUD operations
                    'postOnly + delete', // we only allow deletion via POST request
                    'BackEndSettings + create, update, admin, delete',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        public function init()
        {
            parent::init();
            
            $this->pageTitle = 'Отзывы';
        }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Reviews;
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Reviews']))
		{
			$model->attributes=$_POST['Reviews'];
                        
			if($model->save()){
                            if(isset($_POST['apply']))
                                $this->redirect(array('update', 'id' => $model->id));
                            else
                                $this->redirect(array('admin'));
                        }
			
		}
                if($model->type != 'private') $this->_addScript();
		$this->render('form',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Reviews']))
		{
                    $model->attributes=$_POST['Reviews'];
                    if($model->save()){
                        if(isset($_POST['apply']))
                            $this->redirect(array('update', 'id' => $model->id));
                        else
                            $this->redirect(array('admin'));
                    }
		}
                if($model->type != 'private') $this->_addScript();

		$this->render('form',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $this->run_module('left-top', 'AllServices');
            
            $dataProvider=new CActiveDataProvider('Reviews');
            $dataProvider->pagination->pageSize = 100;
            
            $this->render('index',array(
                'dataProvider'=>$dataProvider,
            ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Reviews('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Reviews']))
			$model->attributes=$_GET['Reviews'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Reviews the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Reviews::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Reviews $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='reviews-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        protected function _addScript()
        {
            Yii::app()->clientScript->registerScript('hide-logo', '
                $(".block_img").hide();
            ');
        }
}
