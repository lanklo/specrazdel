<?php
/* @var $this DefaultController */
/* @var $model Statpages */
/* @var $form CActiveForm */
?>

<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?= $this->pageTitle; ?></h1>
    </div>
</div>

<div class="row-fluid" id="grid">
    <?php $form=$this->beginWidget('AdminForm', array(
        'id'=>'candidates-form',
        'type' => 'horizontal',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    )); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->textFieldRow($model, 'title', array('class'=>'span5')); ?>
    <?= $form->imageFieldRow($model, 'img'); ?>
    <?= $form->textAreaRow($model, 'text', array('class'=>'span5 mceEditor', 'rows' => 15)); ?>
    <?= $form->textFieldRow($model, 'url', array('class'=>'span5')); ?>
    
    <fieldset>
        <legend>СЕО</legend>
        <?= $form->textFieldRow($model->seos, 'title', array('class'=>'span5')); ?>
        <?= $form->textFieldRow($model->seos, 'keywords', array('class'=>'span5')); ?>
        <?= $form->textFieldRow($model->seos, 'description', array('class'=>'span5')); ?>
    </fieldset>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Сохранить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Применить',
            'htmlOptions' => array('name' => 'apply'))
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>