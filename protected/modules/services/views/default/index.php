<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
?>

<h1>Услуги</h1>

<div class="reviews">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
        'template' => '{items}',
)); ?>
</div>
