<?php
/* @var $this DefaultController */
/* @var $data Statpages */
?>

<div class="view">
    <div class="item-img">
        <?= CHtml::image($data->imageThumbUrl, '') ?>
        <div class="lbl"></div>
    </div>
    <div class="item-txt">
        <h3><?= $data->title ?></h3>
    </div>
    <div class="clearfix"></div>
</div>