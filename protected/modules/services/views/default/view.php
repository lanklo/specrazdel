<?php
/* @var $this DefaultController */
/* @var $model Statpages */
?>

<h1><?= $model->title; ?></h1>

<div class="detail-view">
    <?= $model->text ?>
</div>

<?php $this->widget('OurWorks') ?>