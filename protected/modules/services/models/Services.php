<?php

/**
 * This is the model class for table "services".
 *
 * The followings are the available columns in table 'services':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $type
 * @property string $text
 * @property string $img
 * @property string $url
 *
 * The followings are the available model relations:
 * @property Seo[] $seos
 */
class Services extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Statpages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'services';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('title, text', 'required'),
			array('title, url', 'length', 'max'=>255),
			array('create_time, update_time, text', 'safe'),
                        array('url', 'transliterate'),
                        array('url', 'required'),
                        array('url', 'unique'),
                        array('img', 'safe'),
                        array('img', 'file', 
                            'types' => 'gif, png, jpg, jpeg',
                            'allowEmpty' => true,
                        ),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, create_time, update_time, title, text, url', 'safe', 'on'=>'search'),
		);
	}
        
        public function scopes() 
        {
            return array(
                'main' => array(
                    'limit' => 9,
                    'order' => 'id',
                )
            );
        }

        public function defaultScope() 
        {
            return array(
                'order' => 'id DESC',
            );
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'seos' => array(self::HAS_ONE, 'Seo', 'entity_id', 
//                        'condition' => "seos.type=='" . $this->type . "'"
                        ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'title' => 'Заголовок',
			'text' => 'Текст',
			'url' => 'Url',
                        'img' => 'Изображение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function behaviors()
        {
            return array(
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                ),
                'uploadableImage'=>array(
                    'class'=>'application.components.UploadableImageBehavior',
                    'savePathAlias' => 'webroot.uploaded.services',
                    'thumbWidth' => 220,
                ),
            );
        }
        
        public function transliterate($attribute,$params)
        {
            if(empty($this->$attribute) && !empty($this->title))
                $this->$attribute = $this->title;

            $this->$attribute = UrlTransliterate::cleanString($this->$attribute);
        }
        
        public function afterConstruct() 
        {
            parent::afterConstruct();
            $this->seos = new Seo();
        }
        
        public function afterFind() 
        {
            parent::afterFind();
            if(!isset($this->seos)) $this->seos = new Seo();
        }
        
        public function afterSave() 
        {
            parent::afterSave();
            $this->seos->entity_id = $this->id;
            $this->seos->type = 'service';
            $this->seos->save();
            
            return true;
        }
        
        public function getMenu()
        {
            $data = Yii::app()->db->createCommand()
                    ->select('url, title')
                    ->order('id')
                    ->from($this->tableName())
                    ->queryAll();
            $services = array();
            foreach ($data as $item){
                $services[] = array(
                    'label' => $item['title'],
                    'url' => array('/services/default/view', 'url' => $item['url'])
                );
            }
            return $services;
        }
        
        public function getList()
        {
            $cmd = Yii::app()->db->createCommand()
                    ->select('id, title')
                    ->from($this->tableName());
            
            $sections = $cmd->queryAll();
            return CHtml::listData($sections, 'id', 'title');
        }
        
        public function frontsearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('title', $this->title, true, 'OR');
                $criteria->compare('text', $this->text, true, 'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'id DESC'
                        )
		));
	}
}