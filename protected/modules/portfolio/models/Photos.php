<?php

/**
 * This is the model class for table "photos".
 *
 * The followings are the available columns in table 'photos':
 * @property integer $id
 * @property integer $portfolio_id
 * @property string $img
 *
 * The followings are the available model relations:
 * @property Portfolio $portfolio
 */
class Photos extends CActiveRecord
{
        public $index;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Photos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'photos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('portfolio_id', 'numerical', 'integerOnly'=>true),
			array('img', 'length', 'max'=>45),
                        array('img', 'file', 
                            'allowEmpty' => true,
                            'types' => 'gif, png, jpg, jpeg',
                            'maxFiles' => 20,
                        ),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, portfolio_id, file', 'safe', 'on'=>'search'),
		);
	}
        
        public function behaviors(){
            return array(
                'uploadableFile'=>array(
                    'class' => 'application.components.UploadableImageBehavior',
                    'savePathAlias' => 'webroot.uploaded.photos',
                    'thumbWidth' => 220,
                ),
            );
        }
        
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'portfolio' => array(self::BELONGS_TO, 'Portfolio', 'portfolio_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
                    'id' => 'ID',
                    'portfolio_id' => 'Portfolio',
                    'img[]' => 'Изображение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('portfolio_id',$this->portfolio_id);
		$criteria->compare('file',$this->file,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}