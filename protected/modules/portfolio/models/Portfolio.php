<?php

/**
 * This is the model class for table "portfolio".
 *
 * The followings are the available columns in table 'portfolio':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $text
 * @property string $finish_date
 * @property string $city
 * @property string $img
 * @property string $anons
 * @property string $address
 *
 * The followings are the available model relations:
 * @property Photos[] $photos
 */
class Portfolio extends CActiveRecord
{
        public $upload;
        public $maxFiles = 20;
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Portfolio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'portfolio';
	}
        
        public function scopes() 
        {
            return array(
                'random' => array(
                    'order' => 'RAND()',
                ),
            );
        }
        
        public function defaultScope() 
        {
            return array(
                'order' => 'id DESC',
            );
        }
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('title, text, anons, city', 'required'),
			array('title, city, img', 'length', 'max'=>45),
			array('anons, address', 'length', 'max'=>255),
			array('create_time, update_time, text, finish_date', 'safe'),
                        array('upload', 'maxCount', 'value' => 20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, create_time, update_time, title, text, finish_date, city, img, anons, address', 'safe', 'on'=>'search'),
		);
	}
        
        public function maxCount($attribute, $params)
        {
            if(count($this->photos) > $params['value'])
                $this->addError('password',"Максимальное количество фотографий - {$params['value']}");
        }
        
        public function behaviors()
        {
            return array(
                'uploadableFile'=>array(
                    'class'=>'application.components.UploadableImageBehavior',
                    'savePathAlias' => 'webroot.uploaded.portfolio',
                    'imageWidth' => 680,
                    'thumbWidth' => 680,
                ),
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                ),
            );
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'photos' => array(self::HAS_MANY, 'Photos', 'portfolio_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Дата создания',
			'update_time' => 'Дата редактирования',
			'title' => 'Название работы',
			'text' => 'Описание',
			'finish_date' => 'Дата окончания работ',
			'city' => 'Город',
                        'imageUrl' => 'Главное фото',
			'img' => 'Главное фото',
			'anons' => 'Краткое описание',
			'address' => 'Адрес объекта',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('finish_date',$this->finish_date,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('anons',$this->anons,true);
		$criteria->compare('address',$this->address,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function beforeSave() 
        {
            if(parent::beforeSave()){
                if(!$this->_addPhotos()) return false;
                
                $this->_deletePhotos();
            }
            
            return true;
        }
        
        public function afterSave() 
        {
            parent::afterSave();
            $this->_saveFiles();
        }
        
        public function beforeDelete() 
        {
            parent::beforeDelete();
            
            foreach ($this->photos as $photo)
                if(is_object ($photo)) $photo->delete();
            
            return true;
        }
        
        protected function _addPhotos()
        {
            $i = 0;
            $photos = array();
            while($file = CUploadedFile::getInstance('Photos', "img[$i]")){
                $photo = new Photos();
                $photo->index = $i;
                $photo->portfolio_id = $this->id;
                $photo->img = $file->name;
                $photo->scenario = 'multiple';
                $photos[] = $photo;
                $i++;
            }
            
            $valid = true;
            foreach ($photos as $photo){
                $valid = $photo->validate() && $valid;
                if(!$valid){
                    $this->addError('upload', $photo->getError('img'));
                    return false;
                }
            }
            return $valid && $this->_checkUploadCount($photos);
        }
        
        protected function _checkUploadCount($photos)
        {
            $uploadCount = count($this->photos) + count($photos);
            
            if($uploadCount > $this->maxFiles){
                $this->addError('upload', "Максимальное количество фотографий - $this->maxFiles");
                return false;
            }
            $this->photos = $photos;
//            if(!$this->validate(array('upload'))) {
//                return false;
//            }
            return true;
        }

        public function _saveFiles()
        {
            foreach ($this->photos as $file)
                $file->save();
        }
        
        protected function _deletePhotos()
        {
            if(isset($_POST['Photos']['delete']))
                foreach($_POST['Photos']['delete'] as $item){
                    foreach($item as $id => $value){
                        if ($value){
                            $file = Photos::model()->deleteByPk($id);
                            if(is_object($file)) $file->delete();
                        }
                    }
                }
        }
        
        public function frontsearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('title', $this->title, true, 'OR');
                $criteria->compare('text', $this->text, true, 'OR');
                $criteria->compare('city', $this->city, true, 'OR');
                $criteria->compare('anons', $this->anons, true, 'OR');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'id DESC'
                        )
		));
	}
}