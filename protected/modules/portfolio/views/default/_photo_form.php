<?php if($model->isNewRecord): ?>
    Создайте работу для добавления фотографий
<?php else: ?>
    <?= $form->fileFieldRow(new Photos, 'img[]', array(
        'multiple' => 'multiple',
        'accept' => 'image/*',
    )) ?>
    <?php foreach ($model->photos as $item): ?>
        <div class='control-group'>
            <div class='controls'>
                <?= CHtml::image($item->imageThumbUrl,'', array(
                    'class' => 'thumbnail',
                )) ?>
                <label class="checkbox">
                    <input name="<?= get_class($item) ?>[delete][img][<?= $item->id ?>]" id="Img_delete" value="1" type="checkbox">
                    Удалить файл
                </label>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>