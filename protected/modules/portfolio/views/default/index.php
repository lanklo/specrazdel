<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
?>

<h1>Наши работы</h1>

<div class="portfolio" id="listView">
    <?php $this->renderPartial('_list', array(
        'dataProvider' => $dataProvider,
     )) ?>
</div>
<?php $this->widget('AjaxPager', array(
    'dataProvider' => $dataProvider, 
    'url' => '"/portfolio/default/index/"',
    'pageVar' => 'Portfolio_page',
    'text' => '',
)) ?>