<?php
/* @var $this CandidatesController */
/* @var $model Candidates */
/* @var $form CActiveForm */
?>

<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?= $this->pageTitle; ?></h1>
    </div>
</div>

<div class="row-fluid" id="grid">
    <?php $form=$this->beginWidget('AdminForm', array(
        'id'=>'candidates-form',
        'type' => 'horizontal',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data'),
    )); ?>
        <?= $form->errorSummary($model); ?>
        <?php $this->widget('bootstrap.widgets.TbTabs', array(
            'tabs'=> array(
                array(
                    'label' => 'Информация',
                    'active' => true,
                    'content' => $this->renderPartial('_main_form', array(
                        'form' => $form,
                        'model' => $model
                    ), true)),
                array(
                    'label' => 'Фотогалерея',
                    'content' => $this->renderPartial('_photo_form', array(
                        'form' => $form,
                        'model' => $model
                    ), true))
            )
        )); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Сохранить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Применить',
            'htmlOptions' => array('name' => 'apply'))
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>