<?php
/* @var $this DefaultController */
/* @var $model Portfolio */
?>
    
<h1><?= $model->title ?></h1>

<?= CHtml::image($model->imageUrl, $model->title, array('class' => 'detail-picture')) ?>

<div class="orange-icons small-text">
    <?php if($model->city): ?>
    <span class="place"><?= $model->city ?>
        <?php if($model->address): ?>, <?= $model->address ?><?php endif; ?>
    </span>
    <?php endif; ?>
    <span class="calendar"><?= date('d/m/Y', strtotime($model->create_time)) ?></span>
</div>

<div class="detail-view">
    <?= $model->text ?>
</div>
<div class="photos_list">
    <?php foreach ($model->photos as $k => $item): ?>
        <div class="over-img<?= (($k + 1) % 3 == 0 ? ' last' : '') ?>">
        <?= CHtml::image($item->imageThumbUrl, '', array()) ?>
        </div>
    <?php endforeach; ?>
</div>
