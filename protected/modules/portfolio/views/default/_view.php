<?php
/* @var $this DefaultController */
/* @var $data Portfolio */
?>

<div class="view">
    <div class="image-over">
        <?= CHtml::link(CHtml::image($data->imageUrl, $data->title), array(
            'view', 'id'=>$data->id
        )); ?>
    </div>
    <div class="border-block">
        <?= CHtml::link(CHtml::encode($data->title), array('view', 'id'=>$data->id)); ?>
        <p><?= $data->anons ?></p>
        <div class="orange-icons small-text">
            <?php if($data->city): ?>
            <span class="place"><?= $data->city ?>
                <?php if($data->address): ?>, <?= $data->address ?><?php endif; ?>
            </span>
            <?php endif; ?>
            <span class="calendar"><?= date('d/m/Y', strtotime($data->create_time)) ?></span>
        </div>
    </div>
</div>