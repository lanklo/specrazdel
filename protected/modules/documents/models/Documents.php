<?php

/**
 * This is the model class for table "norm_documents".
 *
 * The followings are the available columns in table 'norm_documents':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $file
 * @property integer $section_id
 *
 * The followings are the available model relations:
 * @property Sections $section
 */
class Documents extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Documents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'norm_documents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('title', 'required'),
			array('section_id', 'numerical', 'integerOnly'=>true),
			array('title, file', 'length', 'max'=>255),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, create_time, update_time, title, file, section_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'section' => array(self::BELONGS_TO, 'Sections', 'section_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'title' => 'Заголовок',
			'file' => 'Файл',
			'section_id' => 'Раздел',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
//		$criteria->compare('create_time',$this->create_time,true);
//		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('title',$this->title,true);
//		$criteria->compare('file',$this->file,true);
		$criteria->compare('section_id',$this->section_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function behaviors()
        {
            return array(
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                ),
                'uploadableFile'=>array(
                    'class'=>'application.components.UploadableFileBehavior',
                    'fileTypes' => 'pdf',
                    'savePathAlias' => 'webroot.uploaded.documents',
                ),
            );
        }
        
        public function getSectionName()
        {
            if($this->section_id)
                return $this->section->title;
            
            return false;
        }
        
        public function getFileSize()
        {
            if(!$this->file) return;
            
            $url = Yii::getPathOfAlias('webroot.uploaded.documents').DIRECTORY_SEPARATOR.$this->file;
            if(file_exists($url)){
                $size = filesize($url);
                if($size < 1024) return $size . ' б';
                elseif(($size = $size/1024) < 1024) return round($size) . ' кб';
                elseif(($size = $size/1024) < 1024) return round($size, 2) . ' мб';
                else return round($size, 2) . ' гб';
            }
            return;
        }
        
        public function getFileExtention()
        {
            if(!$this->file) return;
            
            return preg_replace('/(^.*)(\.)/', '$3', $this->file);
        }
        
        public function frontsearch()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'sort' => array(
                            'defaultOrder' => 'id DESC'
                        )
		));
	}
}