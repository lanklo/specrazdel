<?php
/* @var $this DefaultController */
/* @var $data Documents */
?>

<div class="view" id="<?= $data->id ?>">
    <?= $data->title ?><br/>
    <?php if($data->file): ?>
        <?= CHtml::link('', $data->fileUrl, array(
            'target' => 'blank',
            'class' => 'download-link'
        )) ?>
        <span class="size"><?= $data->fileSize ?></span>
    <?php endif; ?>
</div>