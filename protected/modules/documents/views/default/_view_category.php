<?php
/* @var $this DefaultController */
/* @var $data Documents */
?>
<div class="view-category <?= ($total == $index + 1) ? 'last' : '' ?><?= (!$index) ? 'first' : '' ?>">

	<h3><?= $data->title ?></h3>
        
        <?php foreach ($data->documents as $item): ?>
            <?php $this->renderPartial('_view', array('data' => $item)) ?>
        <?php endforeach; ?>
</div>