<?php
/* @var $this DefaultController */
/* @var $dataProvider CActiveDataProvider */
?>

<h1>Нормативные документы</h1>

<div class="list">
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view_category',
        'template' => '{items}{pager}',
        'viewData' => array('total' => $dataProvider->getTotalItemCount()),
)); ?>
</div>