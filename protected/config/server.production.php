<?php

return array(
    'connectionString' => 'mysql:host=localhost;dbname=u0015579_default',
    'emulatePrepare' => true,
    'username' => 'u0015579_default',
    'password' => 'x_K!lQ3O',
    'charset' => 'utf8',
    // включить кэширование схем для улучшения производительности
    'schemaCachingDuration' => 0,
    // включаем профайлер
    'enableProfiling' => true,
    // показываем значения параметров
    'enableParamLogging' => true,
);
