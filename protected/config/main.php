<?php
if(!isset($environment)){
//    $environment = "production";
    $environment = $_SERVER['APPLICATION_ENV'];
}
//print_r(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'messages');exit();
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Спецраздел',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
            'application.models.*',
            'application.components.*',
            'application.widgets.*',
            'application.vendors.*',
            'application.modules.services.models.*',
	),
//        'defaultController'=>'default',
        'language'=>'ru',
        'sourceLanguage' => 'ru',
	'modules'=>array(
            'gii'=>array(
                'class'=>'system.gii.GiiModule',
                'password'=>'gii',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters'=>array('127.0.0.1','::1'),
            ),
            'documents',
            'portfolio',
            'reviews',
            'services',
	),

	// application components
	'components'=>array(
                'language' => 'ru',
                'coreMessages' => array(
                    'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'messages'
                ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName' => false,
			'rules'=>array(
//                                '<module:\w+>/<action:\w+>/<id:\d+>'=>'<module>/default/<action>',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                                'show/<url>'=>'statpage/view/url/<url>',
                                '/about-us'=>'statpage/view/url/about-us',
                                '<module:\w+>/<url>'=>'<module>/default/view',
                                
			),
		),
		'db' => require(dirname(__FILE__) . '/server.' . $environment . '.php'),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
                'bootstrap'=>array(
                    'class'=>'ext.bootstrap.components.Bootstrap',
                ),
                'blockManager' => array(
                    'class' => 'ext.blockManager.BlockManager',
                ),
                'clientScript' => array(
                    'defaultScriptFilePosition' => CClientScript::POS_END,
                    'coreScriptPosition' => CClientScript::POS_END,
                    'scriptMap' => array(
                        'jquery.js' => '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
                        'jquery-ui.min.js' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
                    ),
                    'packages' => array(
                        'tiny_mce' => array(
                            'baseUrl' => '/js/admin/tiny_mce/',
                            'js' => array('tiny_mce.js', 'tiny_initialization.js'),
                        ),
                        'carousel' => array(
                            'baseUrl' => '/js/rcarousel/',
                            'js' => array(
                                'jquery.ui.core.min.js', 
                                'jquery.ui.widget.min.js', 
                                'jquery.ui.rcarousel.min.js'
                            ),
                            'depends' => array('jquery'),
                        ),
                        'selectbox' => array(
                            'baseUrl' => '/js/',
                            'js' => array('jquery.selectbox-0.2.js'),
                            'depends' => array('jquery'),
                        ),
                    ),
                ),
                'format' => array(
                  'class' => 'application.components.CCFormatter',
                ),
                'cache' => array(
                    'class' => 'CFileCache',
                )
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
            'adminEmail'=>'info@specrazdel.ru',
            'fromEmail' => 'info@specrazdel.ru',
            'fromName' => 'Спецраздел инфо',
            'toEmail' => 'info@specrazdel.ru',
            'googleKey' => 'AIzaSyCHjwISNCzh6Jl2UzrU8kVz5SophbXPGlw',
            'google_coord' => '55.856655, 37.437100',
            'frontEmail' => 'info@specrazdel.ru',
            'frontPhone' => '+7 495 6460253',
	),
);