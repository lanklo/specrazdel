<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProjectTest
 *
 * @author kristina
 */
class ProjectTest extends CTestCase
{
    public $fixtures = array(
        'projects' => 'Project',
    );
    
    public function testCRUD()
    {
        $newProjct = new Project;
        $newProjectName = 'Test project 1';
        $newProjct->setAttributes(array(
            'name' => $newProjectName,
            'descrition' => 'Test project number 1',
            'create_time' => date('Y-m-d H:i:s'),
            'create_user_id' => 1,
            'update_time' => date('Y-m-d H:i:s'),
            'update_user_id' => 1,
        ));
        $this->assertTrue($newProjct->save(false));
        
        //read project
        $retrivedProject = Project::model()->findByPk($newProjct->id);
        $this->assertTrue($retrivedProject instanceof Project);
        $this->assertEquals($newProjectName, $retrivedProject->name);
        
        //update
        $updatedProjectName = 'Update test project 1';
        $newProjct->name = $updatedProjectName;
        $this->assertTrue($newProjct->save(false));
        
        //read project back
        $updatedProject = Project::model()->findByPk($newProjct->id);
        $this->assertTrue($updatedProject instanceof Project);
        $this->assertEquals($newProjectName, $updatedProject->name);
        
        $newProjectId = $newProjct->id;
        $this->assertTrue($newProjct->delete());
        $deletedProject = Project::model()->findByPk($newProjectId);
        $this->assertEqual(NULL, $deletedProject);
        
        //return all rows
        $projects = $this->projects;
        $projectOne = $this->projects['project1'];
        $project = $this->projects('project1');
    }
}

