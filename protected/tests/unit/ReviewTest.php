<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReviewTest
 *
 * @author kristina
 */
class ReviewTest extends CTestCase
{
    public $fixtures = array(
        'reviews' => 'Reviews'
    );
    
    public function testCrud()
    {
        $newReview = new Reviews;
        $newReviewName = 'Test Review 1';
        $newReview->setAttributes(
            array(
                'title' => $newReviewName,
                'type' => 'company',
                'text' => 'Тест текст 1',
                'contact' => 'Тест контакт 1',
            )
        );
        $this->assertTrue($newReview->save());
    }
}
