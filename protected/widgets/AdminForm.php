<?php

Yii::import('bootstrap.widgets.TbActiveForm');

class AdminForm extends TbActiveForm
{

    public function imageFieldRow($model, $attribute, $htmlOptions = array())
    {
        if(!isset($htmlOptions['accept'])) $htmlOptions['accept'] = 'image/*';
        
        $input = $this->inputRow(TbInput::TYPE_FILE, $model, $attribute, null, $htmlOptions);

        if(!empty($model->$attribute)){
            $input .= "<div class='control-group'><div class='controls'>";
            $input .= CHtml::image($model->imageThumbUrl);
            if(!$model->isAttributeRequired($attribute)){
                $input .= '<label class="checkbox">
                        <input name="'.get_class($model).'[delete]['.$attribute.']" id="Img_delete" value="1" type="checkbox">
                        Удалить файл
                    </label>';
            }
            $input .= '</div></div>';
        }
        $input = CHtml::tag('div', array('class' => "block_$attribute"), $input);
        return $input;
    }
}
