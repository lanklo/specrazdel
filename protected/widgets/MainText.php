<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MainMenu
 *
 * @author kristina
 */


class MainText extends CWidget
{
    
    public function run()
    {
        $text1 = Statpages::model()->findByAttributes(array('url' => 'main-1'));
        $text2 = Statpages::model()->findByAttributes(array('url' => 'main-2'));
        $this->render('main_text', compact('text1', 'text2'));
    }
}
