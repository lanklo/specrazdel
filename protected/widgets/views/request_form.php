<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div id="request-block">
    <div class="title">
        <h3>Заявка на расчет<br/>
            стоимости проекта</h3>
        <div class="label"></div>
    </div>
    <div class="form-cnt">
        <div class="cnt">
            <?php $form = $this->beginWidget('FrontForm', array(
                'htmlOptions' => array(
                    'id' => 'request-form',
                    'enctype' => 'multipart/form-data',
            ))); ?>
                <?= $form->errorSummary($model) ?>
                <?php if(Yii::app()->user->hasFlash('success')): ?>
                    <p class="flash"><?= Yii::app()->user->getFlash('success') ?></p>
                <?php endif; ?>
                <?= $form->textField($model, 'name') ?>
                <?= $form->textField($model, 'phone') ?>
                <?= $form->textField($model, 'email') ?>
                <?= $form->dropDownList($model, 'section_id', $model->sectionsList, array(
                    'empty' => '',
                    'class' => 'selectbox'
                )) ?>
                
                <label>Прикрепите файл</label>
                <div class="customFile">
                    <?= $form->fileField(new RequestFiles(), 'file[]', array(
                            'multiple' => 'multiple',
                    )) ?>
                </div>
                <?= CHtml::submitButton('Отправить') ?>
                <p><span class="required">*</span> Обязательные поля</p>
                <p>После обработки Вашей заявки, наш сотрудник свяжется 
                    с вами в кратчайшие строки.</p>
            <?php $this->endWidget(); ?>
        </div>
    </div>  
</div>
<? /* ?>
<?php $form = $this->beginWidget('FrontForm', array(
        'htmlOptions' => array(
            'id' => 'request-form',
            'enctype' => 'multipart/form-data',
    ))); ?>
    <h3>Заявка на расчет стоимости проекта</h3>
    <?= $form->errorSummary($model) ?>
    <?php if(Yii::app()->user->hasFlash('success')): ?>
        <?= Yii::app()->user->getFlash('success') ?><br/>
    <?php endif; ?>
    <?= $form->textField($model, 'name') ?><br/>
    <?= $form->textField($model, 'phone') ?><br/>
    <?= $form->textField($model, 'email') ?><br/>
    <?= $form->dropDownList($model, 'section_id', Sections::model()->getList('request'), array(
        'empty' => '',
    )) ?><br/>
    <?= $form->fileField(new RequestFiles(), 'file[]', array(
            'multiple' => 'multiple',
    )) ?><br/>
    <?= CHtml::submitButton('Отправить') ?>
<?php $this->endWidget(); ?> <? */ ?>