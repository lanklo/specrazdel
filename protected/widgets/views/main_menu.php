<ul class="menu">
<?php foreach ($this->items as $k => $item): ?>
    <li class="<?= ($item['active']) ? ' active' : ''?><?php if($k == count($this->items) - 1):?> last<?php endif; ?>">
        <?= $this->renderMenuItem($item) ?>
        <?php if(count($item['items'])): ?>
            <div class="submenu">
                <table>
                    <tr>
                        <td class="col1">
                            <ul>
                                <?php for($i = 0; $i < round(count($item['items']) / 2); $i++): ?>
                                    <li<?= ($item['items'][$i]['active']) ? ' class="active"' : ''?>><?= $this->renderMenuItem($item['items'][$i]) ?></li>
                                <?php endfor; ?>
                            </ul>
                        </td>
                        <td class="col3">
                            <ul>
                                <?php for($i = round(count($item['items']) / 2); $i < count($item['items']); $i++): ?>
                                    <li<?= ($item['items'][$i]['active']) ? ' class="active"' : ''?>><?= $this->renderMenuItem($item['items'][$i]) ?></li>
                                <?php endfor; ?>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        <?php endif; ?>
    </li>
<?php endforeach; ?>
</ul>