<div class="our-works">
    <div class="title">
        <h4>Наши работы</h4>
        <div class="lbl"></div>
    </div>
    <div class="works">
        
        <div id="container">
            <div id="carousel">
                <?php foreach ($works as $item): ?>
                    <div class="item">
                        <div class="img">
                            <?= CHtml::link(CHtml::image($item->imageThumbUrl), 
                                Yii::app()->createUrl('/portfolio/default/view/', array('id' => $item->id))) ?>
                        </div>
                        <?= CHtml::link($item->title, 
                            Yii::app()->createUrl('/portfolio/default/view/', array('id' => $item->id))) ?>
                    </div>
                <?php endforeach; ?>
            </div>
            <a href="#" id="carousel-next"></a>
            <a href="#" id="carousel-prev"></a>
        </div>
        
        <?= CHtml::link('Смотреть все работы', array('/portfolio/'), array(
            'class' => 'orange-link'
        )) ?>
        <div class="clearfix"></div>
    </div>
</div>