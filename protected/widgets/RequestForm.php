<?php

class RequestForm extends CWidget
{
    public function run()
    {
        $model = new PriceRequest('front');
        if(isset($_POST['PriceRequest'])) 
            $this->_request($model);
        
        if(Yii::app()->controller->module->id == 'services' && $url = Yii::app()->request->getParam('url')){
            $service = Services::model()->findByAttributes(array('url' => $url));
            if($service) $model->section_id = $service->id;
        }
        
        $this->render('request_form', array('model' => $model));
    }

    private function _request($model)
    {
        $model->attributes = $_POST['PriceRequest'];
        $model->status = 'new';
        if($model->save()){
            Yii::app()->user->setFlash('success', 
                'Спасибо за вашу заявку. Наши менеджеры рассмотрят вашу заявку в течении 2-х дней и дадут вам ответ.');
            
            $this->_sendMail($model);
            
            Yii::app()->request->redirect(Yii::app()->request->requestUri);
        }
    }
    
    protected function _sendMail($model)
    {
        $url =  Yii::app()->controller->createAbsoluteUrl('/pricerequest/update/', array('id' => $model->id));
        Mail::sendMail(
            "Получен новый заказ - #$model->id",
            "Получена новая заявка на просчет\n\n" .
            "№: $model->id\n" .
            "Имя автора: $model->name \n" . 
            "Телефон автора: $model->phone \n" . 
            "Раздел: $model->sectionName \n\n" . 
            "Ссылка на просмотр заявки: $url \n" . 
            "Дата: " . date('d.m.Y H:i')
        );
        
        Mail::sendMail(
            'Заявка  на просчет',
            'Спасибо за вашу заявку. Наши менеджеры рассмотрят вашу заявку в течении 2-х дней и дадут вам ответ.',
            $model->email
        );
    }
}
