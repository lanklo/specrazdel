<?php

class OurWorks extends CWidget
{
    public function run()
    {
        Yii::import('application.modules.portfolio.models.*');
        Yii::app()->clientScript->registerPackage('carousel');
        
        $works = Portfolio::model()->random()->findAll();
        
        if(!count($works)) return;
            
        $this->render('our_works', array('works' => $works));
    }
}
