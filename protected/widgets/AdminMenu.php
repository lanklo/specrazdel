<?php

class AdminMenu extends CWidget
{
    public static $menu = array(
        array('label' => 'Наши работы'),
        array('label' => 'Наши работы', 'url' => '/portfolio/default/admin'),
        array('label' => 'Страницы', 'url' => '/statpage/admin'),
        array('label' => 'Сервисы', 'url' => '/services/default/admin'),
        array('label' => 'Отзывы', 'url' => '/reviews/default/admin'),
        array('label' => 'Нормативные документы', 'url' => '/documents/default/admin'),
        array('label' => 'Заявки', 'url' => '/pricerequest/admin'),
        array('label' => 'Настройки'),
        array('label' => 'Настройки', 'url' => '/site/settings'),
//        array('label' => 'Категории заявок', 'url' => '/sections/admin/'),
        array('label' => 'Категории нормативных документов', 'url' => '/sections/admin/type/document'),
    );

    public function run()
    {
        foreach(AdminMenu::$menu as $key => $item){
            
            if(isset($item['controller']) && $item['controller'] == Yii::app()->controller->id){
                if(isset($item['action']) && $item['module'] == Yii::app()->controller->module->id){
                    if($item['action'] == Yii::app()->controller->action->id)
                        $item['active'] = true;
                    else $item['active'] = false;
                }else{
                    if(isset($item['module']) && $item['module'] == Yii::app()->controller->module->id || !isset($item['module'])){
                        $item['active'] = true;
                    }
                    if($item['controller'] == 'categories'){
                        if(isset($_GET['category'])){
                            $item['active'] = false;
                            if($item['cat'] == 'sub') $item['active'] = true;
                        }                    
                        if(!isset($_GET['category'])){
                            $item['active'] = false;
                            if($item['cat'] == 'top') $item['active'] = true;
                        }
                    }    
                }
                AdminMenu::$menu[$key] = $item;
            }
        }
        $this->render('adminMenu', array('menu' => AdminMenu::$menu));
    }

}