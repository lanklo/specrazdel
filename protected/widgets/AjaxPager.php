<?php

class AjaxPager extends CWidget
{
    public $dataProvider;
    public $showLoader = true;
    public $text = 'Показати ще';
    public $url = 'window.location.href';
    public $id = 'listView';
    public $addHide = true;
    public $pageVar = 'page';
    
    public function run()
    {
        if ($this->dataProvider->totalItemCount <= $this->dataProvider->pagination->pageSize)
            return;
        
        $script = "
            $('.paginator').hide();
            var page = parseInt('" . ($this->dataProvider->pagination->currentPage + 1) . "');
            var pageCount = parseInt('{$this->dataProvider->pagination->pageCount}');
            var loadingFlag = false;
            var showLoader = $this->showLoader;
            
//            $('#showMore-{$this->id}').click(function()
            $(window).scroll(function (){
                if(LoadElements('#showMore-{$this->id}'))
                {
                    if (!loadingFlag)
                    {
                        loadingFlag = true;
                        var link = $('#showMore-{$this->id}').parent();
                        if(showLoader) link.show();

                        $.ajax({
                            type: 'get',
                            url: {$this->url},
                            data: {
                                '$this->pageVar': page + 1,
                                '" . Yii::app()->request->csrfTokenName . "': '" . Yii::app()->request->csrfToken . "'
                            },
                            success: function(data)
                            {
                                page++;
                                loadingFlag = false;";

            if($this->addHide) $script .= "data = '<div class=\'hide\'>'+data+'</div>';";

            $script .= "$('#{$this->id}').append(data);
                                $('.hide').slideDown(1000, function(){
                                    $(this).removeClass('hide');
                                });
                                link.hide();
                                if (page >= pageCount) link.remove();
                            }
                        });
                    }
                    return false;
    //            });
                }
            });
        ";
        Yii::app()->clientScript->registerScript('pager-' . $this->id, $script);
            
        $this->render('ajaxPager');
    }
}
