<?php

/**
 * This is the model class for table "sections".
 *
 * The followings are the available columns in table 'sections':
 * @property string $title
 * @property string $type
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property Documents[] $documents
 * @property PriceRequest[] $priceRequests
 */
class Sections extends CActiveRecord
{   
        public $types = array(
            'request' => 'Заявка',
            'document' => 'Нормативные документы',
        );
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sections the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sections';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('title, type', 'required'),
			array('title', 'length', 'max'=>255),
			array('type', 'length', 'max'=>8),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('title, type, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'documents' => array(self::HAS_MANY, 'Documents', 'section_id'),
			'priceRequests' => array(self::HAS_MANY, 'PriceRequest', 'section_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'title' => 'Заголовок',
			'type' => 'Type',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('title',$this->title,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getList($type = null)
        {
            $cmd = Yii::app()->db->createCommand()
                    ->select('id, title')
                    ->from($this->tableName());
            
            if($type) $cmd->andWhere ('type=:type', array(':type' => $type));
            
            $sections = $cmd->queryAll();
            return CHtml::listData($sections, 'id', 'title');
        }
}