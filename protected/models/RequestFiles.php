<?php

/**
 * This is the model class for table "request_files".
 *
 * The followings are the available columns in table 'request_files':
 * @property integer $id
 * @property integer $request_id
 * @property string $file
 *
 * The followings are the available model relations:
 * @property PriceRequest $request
 */
class RequestFiles extends CActiveRecord
{
        public $index;

        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RequestFiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'request_files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('request_id', 'numerical', 'integerOnly'=>true),
			array('file', 'length', 'max'=>45),
                        array('file', 'file', 
                            'allowEmpty' => true,
                            'maxSize' => 10 * 1024 * 1024,
                            'maxFiles' => 3,
                            'types' => 'doc, docx, xls, pdf, ppt, jpg, png, zip, rar, jpeg',
                            'message' => 'Размер файлов не может превышать 10 Мб'
                        ),
                        array('file', 'FileSizeCombined',
                            'maxSize' => 10 * 1024 * 1024,
                        ),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, request_id, file', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'request' => array(self::BELONGS_TO, 'PriceRequest', 'request_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'request_id' => 'Request',
			'file' => 'File',
                        'file[]' => 'Выбрать файл',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('request_id',$this->request_id);
		$criteria->compare('file',$this->file,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function behaviors()
        {
            return array(
                'uploadedFile'=>array(
                    'class' => 'application.components.UploadableFileBehavior',
                    'savePathAlias' => 'webroot.uploaded.request_files.' . $this->request_id,
                    'fileTypes' => 'doc, docx, xls, pdf, ppt, jpg, png, zip, rar',
                ),
            );
        }
}
