<?php

/**
 * This is the model class for table "price_request".
 *
 * The followings are the available columns in table 'price_request':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $section_id
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Sections $section
 * @property RequestFiles[] $requestFiles
 */
class PriceRequest extends CActiveRecord
{
        public $statuses = array(
            'new' => 'Необработано',
            'complete' => 'Обработано',
            'closed' => 'Закрыто',
        );
        
        public $upload;
        public $files;

        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PriceRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'price_request';
	}
        
        public function defaultScope() 
        {
            return array(
                'order' => 'id DESC',
            );
        }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('name, phone, email', 'required'),
                        array('status', 'default', 'value' => 'new'),
                        array('email', 'email'),
			array('section_id', 'numerical', 'integerOnly'=>true),
			array('name, phone, email', 'length', 'max'=>45),
			array('status', 'length', 'max'=>8),
			array('create_time, update_time', 'safe'),
//                        array('files', 'file', 
//                            'allowEmpty' => true,
//                            'maxSize' => 10 * 1024 * 1024,
//                            'maxFiles' => 1,
//                            'types' => 'doc, docx, xls, pdf, ppt, jpg, png, zip, rar, jpeg',
//                            'message' => 'Размер файлов не может превышать 10 Мб'
//                        ),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, create_time, update_time, name, phone, email, section_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
//			'section' => array(self::BELONGS_TO, 'Sections', 'section_id'),
                        'section' => array(self::BELONGS_TO, 'Services', 'section_id'),
			'requestFiles' => array(self::HAS_MANY, 'RequestFiles', 'request_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Номер заявки',
			'create_time' => 'Дата получения заявки',
			'update_time' => 'Update Time',
			'name' => 'Фамилия и имя',
			'phone' => 'Телефон',
			'email' => 'Email',
			'section_id' => 'Выберите раздел',
			'status' => 'Статус',
                        'sectionName' => 'Услуга',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function behaviors()
        {
            return array(
                'CTimestampBehavior' => array(
                    'class' => 'zii.behaviors.CTimestampBehavior',
                ),
//                'uploadedFile'=>array(
//                    'class' => 'application.components.UploadableMultipleFileBehavior',
//                    'savePathAlias' => 'webroot.uploaded.request_files.' . $this->id,
//                    'fileTypes' => 'doc, docx, xls, pdf, ppt, jpg, png, zip, rar',
//                    'attributeName' => 'files',
//                ),
            );
        }
        
        public function getSectionName()
        {
            if(isset($this->section))
                return $this->section->title;
            return false;
        }
        
        public function beforeSave() 
        {
            if(parent::beforeSave()){
                if(!$this->_addFiles()){
//                    $this->requestFiles = array();
                    return false;
                }
                
                $this->_deleteFiles();
            }
            
            return true;
        }
        
        public function afterSave() 
        {
            parent::afterSave();
            $this->_saveFiles();
        }
        
        public function beforeDelete() 
        {
            parent::beforeDelete();
            
            foreach ($this->requestFiles as $item)
                if(is_object($item)) $item->delete();
            
            return true;
        }
        
        protected function _addFiles()
        {
            $i = 0;
            $files = array();
            while($upload = CUploadedFile::getInstance('RequestFiles', "file[$i]")){
                $file = new RequestFiles();
                $file->index = $i;
                $file->request_id = $this->id;
                $file->file = $upload->name;
                $file->scenario = 'multiple';
                $files[] = $file;
                $i++;
            }
            $this->requestFiles = $files;
            
            $valid = true;
            foreach ($this->requestFiles as $file){
                $valid = $file->validate() && $valid;
                if(!$valid){
                    $this->addError('upload', $file->getError('file'));
                    return false;
                }
            }
            return $valid;
        }
        
        public function _saveFiles()
        {
            $folder = YiiBase::getPathOfAlias('webroot.uploaded.request_files.'.$this->id);
            if (!file_exists($folder))
                mkdir($folder, 0, true);
            
            foreach ($this->requestFiles as $file){
                if(!$file->request_id) $file->request_id = $this->id;
                $file->disableBehavior('uploadedFile');
                
                $file->attachBehavior('uploadableFile', array(
                    'class' => 'application.components.UploadableFileBehavior',
                    'savePathAlias' => 'webroot.uploaded.request_files.' . $this->id,
                    'fileTypes' => 'doc, docx, xls, pdf, ppt, jpg, png, zip, rar, jpeg',
                ));
                $file->save();
            }
        }

        protected function _deleteFiles()
        {
            if(isset($_POST['RequestFiles']['delete']))
                foreach($_POST['RequestFiles']['delete'] as $item){
                    foreach($item as $id => $value){
                        if ($value){
                            $file = RequestFiles::model()->deleteByPk($id);
                            if(is_object($file)) $file->delete();
                        }
                    }
                }
        }
        
        public function getSectionsList()
        {
//            return Sections::model()->getList('request');
            return Services::model()->getList();
        }
}
