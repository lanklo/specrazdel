<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
                    'captcha'=>array(
                        'class'=>'CCaptchaAction',
                        'backColor'=>0xFFFFFF,
                    ),
                    'page'=>array(
                        'class'=>'CViewAction',
                    ),
		);
	}
        
        public function filters()
	{
		return array(
                    'accessControl', // perform access control for CRUD operations
                    'BackEndSettings + settings, administration',
                    array(
                        'COutputCache + contacts, index',
                        'duration' => '3600',
                        'requestTypes' => array('GET'),
                    )
		);
	}
        
        public function accessRules()
	{
		return array(
                        array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('administration', 'settings'),
				'users'=>array('admin'),
			),
                        array('deny', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('administration', 'settings'),
				'users'=>array('*'),
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
            $this->run_module('page-bottom', 'WorkSteps');
            $this->run_module('page-bottom', 'MainText');
            $this->run_module('left-bottom', 'AllServicesBtn');
            
            $services = Services::model()->main()->findAll();
            $texts = $this->_texts();
            
            $this->render('index', compact('services', 'texts'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
            if($error=Yii::app()->errorHandler->error){
                if(Yii::app()->request->isAjaxRequest)
                    echo $error['message'];
                else
                    $this->render('error', $error);
            }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContacts()
	{
            $this->pageTitle = 'Контакты';
	    $text = Statpages::model()->findByAttributes(array('url' => 'contacts'));
            $this->_addMapsJs();
            $this->render('contact', compact('text'));
	}
        
        public function actionAdministration()
        {
            $this->render('administration');
        }
        
        public function actionSettings()
        {
            $params = Params::model()->findAll();
            
            if(isset($_POST['Params'])){
                foreach ($params as $param){
                    if(isset($_POST['Params'][$param->code])){
                        $param->value = $_POST['Params'][$param->code]['value'];
                        if($param->save()) $this->refresh ();
                    }
                }
            }
            
            $this->render('settings', compact('params'));
        }
        
        public function actionSearch()
        {
            $this->pageTitle = 'Поиск';
            $results = array();
            if(isset($_GET['search'])){
                $search = strtolower(trim($_GET['search']));
                $tables = array('Documents', 'Portfolio', 'Reviews', 'Services', 'Statpages');
                foreach ($tables as $table){
                    $results = array_merge($results, $this->_search($table, $search));
                }
            }
            $this->render('search', compact('search', 'results'));
        }
        
        /**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

        protected function _search($table, $search)
        {
            Yii::import('application.modules.' . strtolower($table) . '.models.*');
            $model = new $table;
            foreach ($model->attributeNames() as $attr)
                $model->$attr = $search;
            
            $provider = $model->frontsearch();
            $provider->pagination->pageSize = 100;
            $data = $provider->getData();
            
            $result = array();
            foreach($data as $item){
                if($item->hasAttribute('url')){
                    if($table == 'Statpages'){
                        switch ($item->url){
                            case 'main-1':
                            case 'main-2':
                                $url = '/';
                                break;
                            default:
                                $url = '/site/' . $item->url;
                        }
                    }else
                        $url = Yii::app()->createUrl(strtolower($table) . '/default/view/', array(
                            'url' => $item->url
                        ));
                }else{
                    switch ($table){
                        case 'Documents':
                        case 'Reviews':
                            $url = "/" . strtolower($table) . "#$item->id";
                            break;
                        default:
                            $url = Yii::app()->createUrl(strtolower($table) . '/default/view/', array('id' => $item->id));
                    }
                }
                $result[] = array(
                    'title' => $item->title,
                    'text' => ($item->hasAttribute('text')) ? $item->text : false,
                    'url' => $url,
                );
            }
            return $result;
        }

        protected function _texts()
        {
            $main1 = Statpages::model()->findByAttributes(array('url' => 'main-1'));
            $main2 = Statpages::model()->findByAttributes(array('url' => 'main-2'));
            return array('main-1' => $main1, 'main-2' => $main2);
        }
        
        private function _addMapsJs()
        {
            $key = Yii::app()->params['googleKey'];
            Yii::app()->clientScript->registerScriptFile("//maps.googleapis.com/maps/api/js?key=$key&sensor=false");
            Yii::app()->clientScript->registerScriptFile('/js/googlemap_front.js');
            
            $googleParam = Params::model()->findByPk('google-map');
            $map = isset($googleParam) ? $googleParam->value : Yii::app()->params->google_coord;
            Yii::app()->clientScript->registerScript('map', "show_map($map)");
        }
}
