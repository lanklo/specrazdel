<?php   

class BlockManager extends CWidget {
    
    /**
     *  Папка, где лежат т.н. блоки (виджеты)
     */
    public $folder = 'application.blocks.';
    
    private $blocks = array();
    
    private $content = array();
    
    
    /**
     * Метод загружает нужный блок и возвращает результат его работы
     * @param string $className имя модуля, относительно папки self::folder
	 * @param array $properties свойства модуля
     * @return mixed результат работы модуля
     */
    private function _execute($className, $properties  = array()){
        
        if(empty($className)) return '';
        
        
        
        if(is_array($className)){
            
            $html = '';
            
            foreach($className as $module){

                $name = key($module);
                
                //Если параметры не переданы, то возвращаем пустой массив
                $params = ($module[$name] != $name)? $module[$name]: array();
                
                $html .= $this->_execute($name, $params);
  
            }
            
            return $html;
        }
        
        $separator = (strpos($className,'.'))? '.': '/';

        $path = explode($separator, $className);
            
        $path[] = ucfirst(array_pop($path));

        $className = $this->folder.implode('.',$path);
      
        unset($path);
        
        ob_start();
        ob_implicit_flush(false);
        
        //Создаем виджет
        $widget=Yii::app()->getWidgetFactory()->createWidget($this->owner, $className, $properties);
		$widget->init(); //Инициализируем
        $widget->run(); //Выполняем
      
        return ob_get_clean(); 
    }

    /**
     * Метод запускает указанный блок (виджет)
     * 
     */
    public function module($className, $properties  = array()){
        
        return $this->_execute($className, $properties);
    }

    /**
     * Метод добавляет новый блок
     */
    public function addRun($position, $className, $properties  = array()){

        $content = $this->_execute($className, $properties);
        if(isset($this->content[$position])){
            
            $this->content[$position] .= $content;
        }
        else{
            
            $this->content[$position] = $content;
        }
        return $content;
    }
    
    /**
     * Метод запускает группу блоков (виджетов)
     */
    public function getContent($position){

        return (isset($this->content[$position])) ? $this->content[$position] : '';
        
    }

    /**
     * Метод добавляет новый блок
     */
    public function delete($position){
        
        if(!empty($position) && isset($this->content[$position])){
            unset($this->content[$position]);
        }
    }
    
    /**
     * Метод добавляет новый блок
     */
    public function clear(){
        
        $this->content = array();
    }

    /**
     * Метод проверяет существование блоков
     */
    public function checkModules($position){
        
        return isset($this->content[$position]);
    }

    /**
     * Метод возвращает группу блоков (виджетов)
     */
    public function getModules($position='') {
        
        return (!empty($position) && isset($this->blocks[$position]))? $this->blocks[$position]: $this->blocks;
        
    }
  
               
}