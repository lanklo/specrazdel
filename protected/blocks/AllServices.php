<?php

class AllServices extends Widget
{
    public $items;
    
    public function run()
    {
        Yii::import('application.modules.services.models.*');
        
        $this->items = Services::model()->getMenu();
        
        $this->render('all_services');
    }
}
