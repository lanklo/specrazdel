<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196">
    <link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
</head>
<body>
    <div id="wrapper">
        <div id="header">
            <div id="main-menu">
                <div class="container">
                    <?php $this->widget('MainMenu',array(
                        'items'=> $this->menu,
                        'lastItemCssClass' => 'last',
                        'htmlOptions' => array('class' => 'menu'),
                        'submenuHtmlOptions' => array('class' => 'submenu'),
                        'activeCssClass' => 'css',
                    )); ?>
                </div>
            </div>
            <div id="top-line">
                <a href="/" id="logo">
                    <img src="/images/logo.png"/>
                </a>
                <form action="/site/search" class="right">
                    <div class="search-button">
                        <input name="search" type="text" value=""/>
                        <input type="submit" value=""/>
                    </div>
                </form>
                <div class="right">
                    <span class="icon-phone"></span>
                    <span class="icon-txt"><?= Yii::app()->params['frontPhone'] ?></span>
                    <span class="icon-mail"></span>
                    <span class="icon-txt"><?= Yii::app()->params['frontEmail'] ?></span>
                </div>
            </div>
            <div id="icon-line">
                <div class="container">
                    <ul>
                        <li class="item-term">
                            Срок разработки<br/>
                            от 7 дней
                        </li>
                        <li class="item-people">
                            Специалисты<br/>
                            с 10 летним опытом
                        </li>
                        <li class="item-garanty">
                            100% гарантия
                        </li>
                        <li class="item-price">
                            расчет цены
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container<?= $this->class ? ' page-' . $this->class : '' ?>">
            <div class="content">
                <?= $content ?>
            </div>
            <div class="column">
                <?= $this->getContent('left-top') ?>
                
                <?php $this->widget('RequestForm') ?>
                
                <?= $this->getContent('left-bottom') ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <?= $this->getContent('page-bottom') ?>
    </div>
    <div id="footer">
        <div class="container">
            <?php $this->widget('zii.widgets.CMenu',array(
                'items'=> $this->bottom_menu,
                'htmlOptions' => array('id' => 'footer-menu'),
                'lastItemCssClass' => 'last',
            )); ?>
            <div class="copyright">© 2014, СПЕЦРАЗДЕЛ</div>
            <div class="icons">
                <span class="icon-phone"></span>
                <span class="icon-txt"><?= Yii::app()->params['frontPhone'] ?></span>
                <span class="icon-mail"></span>
                <span class="icon-txt"><?= Yii::app()->params['frontEmail'] ?></span>
            </div>
        </div>
    </div>
</body>
</html>
