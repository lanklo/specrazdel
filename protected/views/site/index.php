<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div class="services">
    <?php foreach($services as $k => $item): ?>
        <div class="item <?= (($k + 1) % 3 == 0) ? 'last' : ''?>">
            <?= CHtml::link(CHtml::image($item->imageThumbUrl, $item->title, array(
                    'width' => '220', 
                    'height' => '147'
                )), 
                array(
                    '/services/default/view/', 
                    'url' => $item->url
            )) ?>
            <div class="title">
                <?= CHtml::link($item->title, array('/services/default/view/', 'url' => $item->url)) ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>