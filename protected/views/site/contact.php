<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */
?>

<h1><?= $this->pageTitle ?></h1>

<div id="googlemap">
    <div id="map_canvas"></div>
</div>
<?php if($text): ?>
    <h2><?= $text->title ?></h2>
    <div class="orange-icons">
        <?= $text->text ?>
    </div>
<?php endif; ?>
