<h1><?= $this->pageTitle ?></h1>

<p>Результаты поиска по запросу "<?= $search ?>"</p>

<div class="search-list">
    <?php if(count($results)): ?>
        <?php foreach($results as $item): ?>
            <div class="item">
                <a href="<?= $item['url'] ?>"><?= $item['title'] ?></a>
                <p><?= $item['text'] ?></p>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <br/><p>Результатов не обнаружено</p>
    <?php endif; ?>
</div>