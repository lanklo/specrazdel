<div style="float:left; width:47%; margin: 0 5% 0 0">
    <?php if(isset($texts['main-1'])): ?>
        <h3><?= $texts['main-1']->title ?></h3>
        <div>
            <?= $texts['main-1']->text ?>
        </div>
    <?php endif; ?>
</div>
<div style="float:left; width:47%">
    <?php if(isset($texts['main-2'])): ?>
        <h3><?= $texts['main-2']->title ?></h3>
        <div><?= $texts['main-2']->text ?></div>
    <?php endif; ?>
</div>
<br clear="all"/>