<div class="work-steps">
    <div class="container">
        <div class="title">
            Этапы разработки<br/>
            проектной документации
        </div>
        <div class="line-change"></div>
        <div class="blue-item">исходные данные</div>
        <div class="blue-item">разработка</div>
        <div class="blue-item">согласование</div>
        <div class="blue-item last">экспертиза</div>
    </div>
    <div class="blue-line"></div>
</div>