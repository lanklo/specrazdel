<div class="row-fluid">
    <div class="page-header container-fluid">
        <h1>Настройки</h1>
    </div>
    <div class="row-fluid" id="grid">
        <?php $form=$this->beginWidget('AdminForm', array(
            'type' => 'horizontal',
        )); ?>
            <?php foreach ($params as $param): ?>
                <?= $form->uneditableRow($param, "code", array('class'=>'span5')); ?>
                <?= $form->textFieldRow($param, "[$param->code]value", array('class'=>'span5')); ?>
                <br/>
            <?php endforeach; ?>
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'primary',
                    'label'=>'Сохранить',
                    'htmlOptions' => array('name' => 'save'))
                ); ?>
            </div>
        <?php $this->endWidget(); ?>
    </div>
</div>