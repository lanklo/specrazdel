<?php
/* @var $this DefaultController */
/* @var $model Statpages */
/* @var $form CActiveForm */
?>

<div class="row-fluid">
    <div class="page-header container-fluid">
	<h1 class="pull-left"><?= $this->pageTitle; ?></h1>
    </div>
</div>

<div class="row-fluid" id="grid">
    <?php $form=$this->beginWidget('AdminForm', array(
        'id'=>'candidates-form',
        'type' => 'horizontal',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        )
    )); ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->dropDownListRow($model, 'status', $model->statuses, array(
        'empty' => '',
        'class'=>'span5'
        )); ?>
    <?php $this->widget('bootstrap.widgets.TbDetailView', array(
        'data'=>$model,
        'attributes'=>array(
            array(
                'name' => 'create_time',
                'type' => 'date',
            ),
            'name',
            'phone',
            'email',
            'sectionName'
        ),
    )); ?>
    <? /* ?>
    <?= $form->textFieldRow($model, 'name', array('class'=>'span5')); ?>
    <?= $form->textFieldRow($model, 'phone', array('class'=>'span5')); ?>
    <?= $form->textFieldRow($model, 'email', array('class'=>'span5')); ?>
    <?= $form->dropDownListRow($model, 'section_id', $model->sectionsList, array(
        'empty' => '',
        'class'=>'span5'
    )); ?>
    <? */ ?>
    <fieldset>
        <legend>Файлы</legend>
        <? /*= $form->fileFieldRow(new RequestFiles(), 'file[]', array(
            'multiple' => 'multiple',
        )) */?>
        <?php if(count($model->requestFiles)): ?>
            <?php foreach ($model->requestFiles as $item): ?>
                <div class='control-group'>
                    <div class='controls'>
                        <?= CHtml::link($item->file, $item->fileUrl, array(
                            'target' => '_blank',
                        )) ?>
                        <label class="checkbox">
                            <input name="<?= get_class($item) ?>[delete][file][<?= $item->id ?>]" id="File_delete" value="1" type="checkbox">
                            Удалить файл
                        </label>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <p>Нет загруженных файлов</p>
        <?php endif; ?>
    </fieldset>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Сохранить',
            'htmlOptions' => array('name' => 'save'))
        ); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'label'=>'Применить',
            'htmlOptions' => array('name' => 'apply'))
        ); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>