<?php
/* @var $this PriceRequestController */
/* @var $model PriceRequest */

$this->breadcrumbs=array(
	'Price Requests'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PriceRequest', 'url'=>array('index')),
	array('label'=>'Create PriceRequest', 'url'=>array('create')),
	array('label'=>'Update PriceRequest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PriceRequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PriceRequest', 'url'=>array('admin')),
);
?>

<h1>View PriceRequest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'create_time',
		'update_time',
		'name',
		'phone',
		'email',
		'section_id',
		'status',
	),
)); ?>
