<?php
/* @var $this PriceRequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Price Requests',
);

$this->menu=array(
	array('label'=>'Create PriceRequest', 'url'=>array('create')),
	array('label'=>'Manage PriceRequest', 'url'=>array('admin')),
);
?>

<h1>Price Requests</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
