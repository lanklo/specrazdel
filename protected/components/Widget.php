<?php

class Widget extends CWidget{
    
    
    private $template;
    
    public $view;
    
    public $params = array();
    
    public $condition = '';
    
    public $limit = 0;
    
    public $order = '';
    
    public $select = '*';
    
    /**
	 * Looks for the view script file according to the view name.
	 * This method will look for the view under the widget's {@link getViewPath viewPath}.
	 * The view script file is named as "ViewName.php". A localized view file
	 * may be returned if internationalization is needed. See {@link CApplication::findLocalizedFile}
	 * for more details.
	 * The view name can also refer to a path alias if it contains dot characters.
	 * @param string $viewName name of the view (without file extension)
	 * @return string the view file path. False if the view file does not exist
	 * @see CApplication::findLocalizedFile
	 */
	public function getViewFile($viewName)
	{
		if(($renderer=Yii::app()->getViewRenderer())!==null)
			$extension=$renderer->fileExtension;
		else
			$extension='.php';
		if(strpos($viewName,'.')) // a path alias
			$viewFile=Yii::getPathOfAlias($viewName);
		else
		{
			$viewFile=$this->getViewPath(true).DIRECTORY_SEPARATOR.$viewName;
			if(is_file($viewFile.$extension)){
			 
                $this->template = $viewFile.$extension;
                
                return Yii::app()->findLocalizedFile($viewFile.$extension);
			}				
			else if($extension!=='.php' && is_file($viewFile.'.php')){
                
                $this->template = $viewFile.'.php';
                
                return Yii::app()->findLocalizedFile($viewFile.'.php');
			}	
			$viewFile=$this->getViewPath(false).DIRECTORY_SEPARATOR.$viewName;
		}

        $this->template = $viewFile.$extension;
        
		if(is_file($viewFile.$extension))
			return Yii::app()->findLocalizedFile($viewFile.$extension);
		else if($extension!=='.php' && is_file($viewFile.'.php'))
			return Yii::app()->findLocalizedFile($viewFile.'.php');
		else
			return false;
	}
    
    
    public function getTemplateFile(){
        
        return $this->template;
    }
}