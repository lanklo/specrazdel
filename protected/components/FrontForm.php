<?php

class FrontForm extends CActiveForm
{
    public function error($model,$attribute,$htmlOptions=array(),$enableAjaxValidation=true,$enableClientValidation=true)
    {
        $html=HtmlForm::error($model,$attribute,$htmlOptions);
        return $html;
    }

    public function textField($model,$attribute,$htmlOptions=array())
    {
        $label = $this->_getLabel($model, $attribute);
        $html = CHtml::activeTextField($model, $attribute, $htmlOptions);
//        if($model->{$attribute} && !$model->hasErrors($attribute)){
//            $html.='<label class="status ok '.$attribute.'">&nbsp;</label>';
//        }
        return $label . $html;
    }

    public function fileField($model,$attribute,$htmlOptions=array())
    {
        $label = $this->_getLabel($model, $attribute);
        $html = CHtml::activeFileField($model,$attribute,$htmlOptions);
        return $label . $html;
    }
    
    public function DropDownList($model, $attribute, $data = array(), $htmlOptions=array())
    {
        $label = $this->_getLabel($model, $attribute);
        $html = CHtml::activeDropDownList($model, $attribute, $data, $htmlOptions);
        return $label . $html;
    }
    
    protected function _getLabel($model, $attribute)
    {
        $label = $model->getAttributeLabel($attribute);
        if($model->isAttributeRequired($attribute))
            $label .= '<span>*</span>';
        return CHtml::tag('label', array('for' => get_class($model) . '_' . $attribute), $label);
    }
}
