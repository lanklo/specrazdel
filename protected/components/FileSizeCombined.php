<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of fileSizeCombined
 *
 * @author kristina
 */
class FileSizeCombined extends CValidator
{
    public $maxSize;
    public $tooLarge;
    private $_filesSize = 0;
    /**
    * Validates the attribute of the object.
    * If there is any error, the error message is added to the object.
    * @param CModel $object the object being validated
    * @param string $attribute the attribute being validated
    */
   protected function validateAttribute($object,$attribute)
   {
        $files=$object->$attribute;
        if(!is_array($files) || !isset($files[0]) || !$files[0] instanceof CUploadedFile)
            $files = CUploadedFile::getInstances($object, $attribute);
        if(array()===$files)
                return $this->emptyAttribute($object, $attribute);
        
        foreach($files as $file)
            $this->getFileSize($object, $attribute, $file);
        
        if($this->_filesSize > $this->maxSize){
            $message=$this->tooLarge!==null?$this->tooLarge : Yii::t('yii','Files are too large. Its size cannot exceed {limit} bytes.');
            $this->addError($object,$attribute,$message,array('{file}'=>CHtml::encode($file->getName()), '{limit}'=>$this->maxSize));
        }
        
   }
   
    protected function getFileSize($object, $attribute, $file)
    {
            $error=(null===$file ? null : $file->getError());
            
            if($error!==UPLOAD_ERR_OK)
            {
                    if($error==UPLOAD_ERR_NO_FILE)
                            return $this->emptyAttribute($object, $attribute);
                    elseif($error==UPLOAD_ERR_PARTIAL)
                            throw new CException(Yii::t('yii','The file "{file}" was only partially uploaded.',array('{file}'=>CHtml::encode($file->getName()))));
                    elseif($error==UPLOAD_ERR_NO_TMP_DIR)
                            throw new CException(Yii::t('yii','Missing the temporary folder to store the uploaded file "{file}".',array('{file}'=>CHtml::encode($file->getName()))));
                    elseif($error==UPLOAD_ERR_CANT_WRITE)
                            throw new CException(Yii::t('yii','Failed to write the uploaded file "{file}" to disk.',array('{file}'=>CHtml::encode($file->getName()))));
                    elseif(defined('UPLOAD_ERR_EXTENSION') && $error==UPLOAD_ERR_EXTENSION)  // available for PHP 5.2.0 or above
                            throw new CException(Yii::t('yii','A PHP extension stopped the file upload.'));
                    else
                            throw new CException(Yii::t('yii','Unable to upload the file "{file}" because of an unrecognized error.',array('{file}'=>CHtml::encode($file->getName()))));
            }
            elseif($error==UPLOAD_ERR_INI_SIZE || $error==UPLOAD_ERR_FORM_SIZE || $this->maxSize!==null && $file->getSize()>$this->maxSize)
            {
                    $message=$this->tooLarge!==null?$this->tooLarge : Yii::t('yii','The file "{file}" is too large. Its size cannot exceed {limit} bytes.');
                    $this->addError($object,$attribute,$message,array('{file}'=>CHtml::encode($file->getName()), '{limit}'=>$this->maxSize));
                    if($error!==UPLOAD_ERR_OK)
                            return;
            }else{
                $this->_filesSize += $file->getSize();
            }
    }
}
