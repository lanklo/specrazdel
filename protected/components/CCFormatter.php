<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CCFormatter
 *
 * @author kristina
 */
class CCFormatter extends CFormatter
{
    public $numOfWords = 5;
    
    public function formatShortText($value)
    {
        $value = CHtml::encode($value);
        $lenBefore = strlen($value);
        if($this->numOfWords){
            if(preg_match("/\s*(\S+\s*){0,$this->numOfWords}/", $value, $match))
                $value = trim($match[0]);
            if(strlen($value) != $lenBefore)
                $value .= '...';
        }
        return $value;
    }
    
    public function formatDate($value)
    {
        $value = date('d.m.Y H:i', strtotime($value));
        return $value;
    }
}
