<?php

/**
 * @property string $savePath путь к директории, в которой сохраняем файлы
 */
class UploadableImageBehavior extends CActiveRecordBehavior{
    /**
     * @var string название атрибута, хранящего в себе имя файла и файл
     */
    public $attributeName = 'img';
    /**
     * @var string алиас директории, куда будем сохранять файлы
     */
    public $savePathAlias = 'webroot.uploaded';
    /**
     * @var array сценарии валидации к которым будут добавлены правила валидации
     * загрузки файлов
     */
    public $scenarios = array('insert', 'update', 'multiple');
    /**
     * @var string типы файлов, которые можно загружать (нужно для валидации)
     */
    public $fileTypes = 'gif, jpeg, jpg, png';
 
    public $name;
    
    public $thumbWidth;
    public $imageWidth;

    /**
     * Шорткат для Yii::getPathOfAlias($this->savePathAlias).DIRECTORY_SEPARATOR.
     * Возвращает путь к директории, в которой будут сохраняться файлы.
     * @return string путь к директории, в которой сохраняем файлы
     */
    public function getSavePath()
    {
        return Yii::getPathOfAlias($this->savePathAlias).DIRECTORY_SEPARATOR;
    }
 
    public function attach($owner){
        parent::attach($owner);
 
        if(in_array($owner->scenario,$this->scenarios)){
            // добавляем валидатор файла
            $fileValidator=CValidator::createValidator('file',$owner,$this->attributeName,
                array('types'=>$this->fileTypes,'allowEmpty'=>true));
            $owner->validatorList->add($fileValidator);
        }
        return true;
    }
 
    // имейте ввиду, что методы-обработчики событий в поведениях должны иметь
    // public-доступ начиная с 1.1.13RC
    public function beforeSave($event)
    {
        $name = $this->attributeName;
        if($this->owner->scenario == 'multiple') 
            $name .= '['.$this->owner->index.']';
        
        if(in_array($this->owner->scenario,$this->scenarios) &&
            CUploadedFile::getInstance($this->owner,$name) &&
            ($file=EUploadedImage::getInstance($this->owner,$name)) && $file->name){
            $this->deleteFile(); // старый файл удалим, потому что загружаем новый
            if($this->name)
                $fname = $this->name . preg_replace('/(^.*)(\.)/', '$2', $file->name);
            else
                $fname = rand() . time() . preg_replace('/(^.*)(\.)/', '$2', $file->name);
            
            if($this->imageWidth) $file->maxWidth = $this->imageWidth;
            
            $this->owner->setAttribute($this->attributeName, $fname);
            if($this->thumbWidth){
                $file->thumb = array(
                    'maxWidth' => $this->thumbWidth,
                    'prefix' => '',
                    'dir' => 't',
                );
            }
            $file->saveAs($this->savePath.$fname);
            
        }elseif($_POST[get_class($this->owner)]['delete'][$this->attributeName]){
            $this->deleteFile();
        }
        return true;
    }
 
    // имейте ввиду, что методы-обработчики событий в поведениях должны иметь
    // public-доступ начиная с 1.1.13RC
    public function beforeDelete($event){
        $this->deleteFile(); // удалили модель? удаляем и файл, связанный с ней
    }
 
    public function deleteFile(){
        $filePath=$this->savePath.$this->owner->getAttribute($this->attributeName);
        if(@is_file($filePath))
            @unlink($filePath);
        
        $filePath=$this->savePath.'t'.DIRECTORY_SEPARATOR.$this->owner->getAttribute($this->attributeName);
        if(@is_file($filePath))
            @unlink($filePath);
    }
    
    public function getImageUrl(){
        return $this->_getBaseImagePath() . $this->owner->getAttribute($this->attributeName);
    }

    public function getImageThumbUrl(){
        return $this->_getBaseImagePath() . 't/' . $this->owner->getAttribute($this->attributeName);
    }

    private function _getBaseImagePath(){
        $path = explode('.', $this->savePathAlias);
        unset($path[0]);
        return Yii::app()->baseUrl . '/' . implode('/', $path) . '/';
    }
}