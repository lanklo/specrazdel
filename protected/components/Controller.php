<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
        public $bottom_menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
        public $class;
        
        public function filterBackEndSettings($filterChain)
        {
//            if (Yii::app()->user->checkRole('admin')){
//                    Yii::app()->bootstrap->register();
                Yii::app()->getComponent('bootstrap');
                $this->pageTitle = 'Панель управления';
                $this->layout = '//layouts/admin';
                $filterChain->run();
//            } else {
//                $this->redirect(Yii::app()->request->baseUrl . '/');
//            }
        }
        
        public function beforeAction($action)
        {
            parent::beforeAction($action);
            
            if ($this->layout == '//layouts/admin') {
                $cs = Yii::app()->clientScript;
                $cs->registerPackage('tiny_mce');
                $cs->registerScriptFile(Yii::app()->baseUrl . '/js/admin/admin.js');
            }else{
                $this->_setClass();
                $this->_addCss();
                $this->_addJs();
                $this->_setMenu();
            }
            return true;
        }
        
        protected function _setClass()
        {
            $this->class = Yii::app()->controller->module->id;
        }
        
        protected function _addCss()
        {
            $cs = Yii::app()->clientScript;
            $cs->registerCssFile(Yii::app()->request->baseUrl . '/css/style.css');
        }
        
        protected function _addJs()
        {
            $cs = Yii::app()->clientScript;
            $cs->scriptMap['jquery.ba-bbq.js'] = false;
            $cs->registerPackage('selectbox');
            $cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/script-min.js');
        }

        protected function _setMenu()
        {
            $services = Services::model()->getMenu();
            $this->bottom_menu = $this->menu = array(
                array('label'=>'О нас', 'url' => array('/about-us')),
                array(
                    'label'=>'Услуги', 
                    'items' => $services,
                ),
                array('label'=>'Наши работы', 'url'=>array('/portfolio')),
                array('label'=>'Отзывы', 'url'=>array('/reviews')),
                array('label'=>'Нормативные документы', 'url'=>array('/documents')),
                array('label'=>'Контакты', 'url'=>array('/site/contacts')),
//                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
//                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            );
            $this->bottom_menu[1]['items'] = array();
            $this->bottom_menu[1]['url'] = array('/services');
            $this->setMenuActive();
        }
        
        protected function setMenuActive()
        {
            foreach($this->menu as $k => $item){
                if(is_array($item['items'])){
                    foreach($item['items'] as $i => $it){
                        if(is_array($it['url'])){
                            $url = Yii::app()->controller->createUrl($it['url'][0], array(
                                'url' => $it['url']['url'])
                            );
                            if(strpos(Yii::app()->request->getUrl(), $url) !== false){
                                $this->menu[$k]['items'][$i]['active'] = true;
                            }
                        }
                    }
                }elseif(is_array($item['url'])){
                    $url = Yii::app()->controller->createUrl($item['url'][0]);
                    if(strpos(Yii::app()->request->getUrl(), $url) !== false){
                        $this->menu[$k]['active'] = true;
                    }
                }
            }
        }
        
        protected function _setSeo($model)
        {
            if(!is_object($model->seos)) return;
            
            $cs = Yii::app()->clientScript;
            $cs->registerMetaTag($model->seos->keywords, 'keywords');
            $cs->registerMetaTag($model->seos->description, 'description');
            
            $title = $model->seos->title ? $model->seos->title : $model->title;
            $this->setTitle($title);
        }
        
        public function setTitle($title)
        {
            $this->pageTitle = $title . ' | ' . Yii::app()->name;
        }
        
        
        /**
        * Метод запускает указанный модуль (виджет)
        *
        */
       public function module($className, $properties  = array())
       {
           return Yii::app()->blockManager->module($className, $properties);
       }

       /**
        * Метод добавляет новый модуль
        */
       public function run_module($position, $className, $properties = array())
       {
           return Yii::app()->blockManager->addRun($position, $className, $properties);
       }

        /**
        * Метод запускает группу модулей (виджетов)
        */
       public function getContent($position)
       {
           return Yii::app()->blockManager->getContent($position);
       }

       /**
        * Метод проверяет существование модулей
        */
       public function checkModules($position)
       {
           return Yii::app()->blockManager->checkModules($position);
       }


       /**
        * Метод возвращает группу модулей (виджетов)
        */
       public function getModules($position = '')
       {
           return Yii::app()->blockManager->getModules($position);
       }

       public function modules_render($view = '', $data = null, $return = false)
       {
           if ($this->beforeRender($view)) {
               $output = '';
               if (!empty($view)) $output=$this->renderPartial($view, $data, true);

               if (($layoutFile=$this->getLayoutFile($this->layout)) !== false) {

                   $output=$this->renderFile($layoutFile, array('content' => $output,), true);
               }

               $this->afterRender($view,$output);

               $output=$this->processOutput($output);

               if ($return)
                   return $output;
               else
                   echo $output;
           }
       }
}
