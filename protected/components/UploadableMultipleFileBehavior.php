<?php

/**
 * @property string $savePath путь к директории, в которой сохраняем файлы
 */
class UploadableMultipleFileBehavior extends CActiveRecordBehavior{
    /**
     * @var string название атрибута, хранящего в себе имя файла и файл
     */
    public $attributeName = 'file';
    /**
     * @var string алиас директории, куда будем сохранять файлы
     */
    public $savePathAlias = 'webroot.uploaded';
    /**
     * @var array сценарии валидации к которым будут добавлены правила валидации
     * загрузки файлов
     */
    public $scenarios = array('insert', 'update', 'multiple');
    /**
     * @var string типы файлов, которые можно загружать (нужно для валидации)
     */
    public $fileTypes = '';
    
    public $validators = array();


    public $name;

    /**
     * Шорткат для Yii::getPathOfAlias($this->savePathAlias).DIRECTORY_SEPARATOR.
     * Возвращает путь к директории, в которой будут сохраняться файлы.
     * @return string путь к директории, в которой сохраняем файлы
     */
    public function getSavePath()
    {
        return Yii::getPathOfAlias($this->savePathAlias).DIRECTORY_SEPARATOR;
    }
 
    public function attach($owner){
        parent::attach($owner);
 
        if(in_array($owner->scenario,$this->scenarios)){
            // добавляем валидатор файла
            
            $validators = array('types'=>$this->fileTypes, 'allowEmpty'=>true);
            $validators = array_merge($validators, $this->validators);
            
            $fileValidator = CValidator::createValidator('file',$owner,$this->attributeName,
                $validators);
            $owner->validatorList->add($fileValidator);
        }
        return true;
    }
 
    // имейте ввиду, что методы-обработчики событий в поведениях должны иметь
    // public-доступ начиная с 1.1.13RC
    public function afterSave($event)
    {
        $attributeName = $this->attributeName;
//        if($this->owner->scenario == 'multiple') 
//            $attributeName .= '['.$this->owner->index.']';
        
        if($this->owner->scenario == 'multiple'){
            if(in_array($this->owner->scenario,$this->scenarios) &&
                ($files=CUploadedFile::getInstances($this->owner,$attributeName))){
                
                $this->deleteFile(); // старый файл удалим, потому что загружаем новый
                
                if($this->name)
                    $name = $this->name . preg_replace('/(^.*)(\.)/', '$2', $file->name);
                else{
                    preg_match('/(^.*)(\.)/', $file->name, $match);
                    $name = UrlTransliterate::cleanString($match[1]) . preg_replace('/(^.*)(\.)/', '$2', $file->name);
    //                $name = rand() . time() . preg_replace('/(^.*)(\.)/', '$2', $file->name);
                }

                $this->owner->setAttribute($this->attributeName, $name);
                $file->saveAs($this->savePath.$name);
            }
        }else{
            if(in_array($this->owner->scenario,$this->scenarios) &&
                ($file=CUploadedFile::getInstance($this->owner,$attributeName))){

                $this->deleteFile(); // старый файл удалим, потому что загружаем новый
                if($this->name)
                    $name = $this->name . preg_replace('/(^.*)(\.)/', '$2', $file->name);
                else{
                    preg_match('/(^.*)(\.)/', $file->name, $match);
                    $name = UrlTransliterate::cleanString($match[1]) . preg_replace('/(^.*)(\.)/', '$2', $file->name);
    //                $name = rand() . time() . preg_replace('/(^.*)(\.)/', '$2', $file->name);
                }

                $this->owner->setAttribute($this->attributeName, $name);
                $file->saveAs($this->savePath.$name);
            }
        }
        return true;
    }
 
    // имейте ввиду, что методы-обработчики событий в поведениях должны иметь
    // public-доступ начиная с 1.1.13RC
    public function beforeDelete($event){
        $this->deleteFile(); // удалили модель? удаляем и файл, связанный с ней
    }
 
    public function deleteFile(){
        $filePath=$this->savePath.$this->owner->getAttribute($this->attributeName);
        if(@is_file($filePath))
            @unlink($filePath);
    }

    public function getFileUrl(){
        return $this->_getBaseFilePath() . $this->owner->getAttribute($this->attributeName);
    }

    private function _getBaseFilePath(){
        $path = explode('.', $this->savePathAlias);
        unset($path[0]);
        return Yii::app()->baseUrl . '/' . implode('/', $path) . '/';
    }
}